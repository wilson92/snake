export const setScore = (score) => {
  sessionStorage.setItem('score', score)
};

export const getScore = () => sessionStorage.getItem('score');

