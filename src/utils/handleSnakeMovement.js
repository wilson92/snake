const handleSnakeMovement = ({ x, y }, direction) => {
  let newX;
  let newY;

  if (direction === 'left') {
    newX = x - 16;
    newY = y;
  } else if (direction === 'right') {
    newX = x + 16;
    newY = y;
  } else if (direction === 'up') {
    newX = x;
    newY = y - 16;
  } else if (direction === 'down') {
    newX = x;
    newY = y + 16;
  }

  return {
    x: newX,
    y: newY
  }
};

export default handleSnakeMovement;
