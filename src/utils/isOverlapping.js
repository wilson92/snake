export default (snake, element) =>(Math.abs(snake.x - element.x) < 16) && (Math.abs(snake.y - element.y) < 16);
