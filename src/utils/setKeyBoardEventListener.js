
const setKeyBoardEventListener = (setDirection, setIskeyPressed) => {
  document.addEventListener("keydown", ({ keyCode }) => {
    if (keyCode === 37) {
      setDirection('left');
    } else if (keyCode === 38) {
      setDirection('up');
    } else if (keyCode === 39) {
      setDirection('right');
    } else if (keyCode === 40) {
      setDirection('down');
    }
    setIskeyPressed(true);
  }, false);

  document.addEventListener("keyup", () => {
    setIskeyPressed(false)
  }, false);
};

export default setKeyBoardEventListener;
