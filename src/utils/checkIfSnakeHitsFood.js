import handleSnakeMovement from './handleSnakeMovement';
import deepCopy from './deepCopy';

const checkIfSnakeHitsFood = (snake, foodPosition, setScore, moveFood, direction) => {
  const isOverlapping = (Math.abs(snake[0].x - foodPosition.x) <= 16) && (Math.abs(snake[0].y - foodPosition.y) <= 16);
  if (isOverlapping) {
    moveFood();
    setScore();
    const snakeLength = snake.length;
    const snakeTail = snake[snakeLength - 1];
    const newSnake = deepCopy(snake);
    newSnake.push({
      id: snakeLength,
      ...handleSnakeMovement(snakeTail, direction)
    })

    return newSnake;
  }
  return snake;
};

export default checkIfSnakeHitsFood;
