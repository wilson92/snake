import React, { useState, useEffect } from 'react';
import styles from './styles.module.scss';
import getRandomNumber from '../../utils/generateRandomNumberBetweenRange';
import Snake from '../Snake';
import {
  setScore as persistScore,
  getScore
} from '../../utils/scorePersitency';

const initialGameData = {
  boardSize: 800,
  score: 0,
};

const calculateFoodPosition = (boardSize, setFoodPosition) => ({
  x: getRandomNumber(0, boardSize),
  y: getRandomNumber(0, boardSize)
});

const Board = () => {
  const [boardSize, setBoardSize] = useState(initialGameData.boardSize);
  const [score, setScore] = useState(initialGameData.score);
  const [foodPosition, setFoodPosition] = useState(calculateFoodPosition(boardSize));
  const [gameOver, setGameOver] = useState(false);

  const callbackSetScore = () => {
    setScore(score + 1);
  };

  const moveFood = () => {
    setFoodPosition(calculateFoodPosition(boardSize));
  };

  const tryAgain = () => {
    setGameOver(false);
    setBoardSize(initialGameData.boardSize);
    setScore(initialGameData.score);
  };

  const verifyHighestScore = () => score > Number(getScore());

  useEffect(() => {
    setTimeout(() => {
      setFoodPosition(calculateFoodPosition(boardSize));
    }, getRandomNumber(4000, 10000))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [foodPosition]);

  useEffect(() => {
    if (gameOver) {
      persistScore(score);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [gameOver])

  return (
    <section style={{
      width: boardSize,
      height: boardSize
    }} className={styles.boardContainer}>

      {!gameOver ? (
        <>
          <span className={styles.score} style={{
            left: (boardSize - 100)
          }}>Score {score}</span>

          {/* Food */}
          <div className={styles.food}
            style={{
              marginTop: foodPosition.y,
              marginLeft: foodPosition.x
            }}> </div>

          <Snake
            boardSize={boardSize}
            setBoardSize={setBoardSize}
            foodPosition={foodPosition}
            setScore={callbackSetScore}
            moveFood={moveFood}
            setGameOver={setGameOver}
          />
        </>

      ) : (
        <>
          {verifyHighestScore  && <h1>It is you highets score {score}</h1>}
          <button className={styles.trayAgainButton} onClick={tryAgain}>Try again</button>
        </>
      )}

    </section>
  )

};

export default Board;