import React, { useState, useEffect } from 'react';
import styles from './styles.module.scss';
import handleSnakeMovement from '../../utils/handleSnakeMovement';
import setKeyBoardEventListener from '../../utils/setKeyBoardEventListener';
import checkIfSnakeHitsFood from '../../utils/checkIfSnakeHitsFood';
import checkIfSnakeHitsItself from '../../utils/checkIfSnakeHitsItself';

const Snake = ({
  boardSize,
  setBoardSize,
  foodPosition,
  setScore,
  moveFood,
  setGameOver
}) => {
  const [snake, setSnake] = useState([{
    id: 0,
    x: boardSize / 2,
    y: boardSize / 2
  }]);
  const [direction, setDirection] = useState('');
  const [iskeyPressed, setIskeyPressed] = useState(false);

  useEffect(() => {
    setKeyBoardEventListener(setDirection, setIskeyPressed);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (iskeyPressed) {
      const newSnake = snake.map((portion, index) => {
        let position;

        if (index === 0) {
          position = handleSnakeMovement(portion, direction);
        } else {
          position = snake[index - 1];
        }

        return {
          ...position,
          id: portion.id
        }
      });

      let finalSnake = checkIfSnakeHitsFood(newSnake, foodPosition, setScore, moveFood, direction);

      if (checkIfSnakeHitsItself(snake)) {
        setGameOver(true);
      }

      setSnake([...finalSnake]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [direction, iskeyPressed])

  return (
    <section className={styles.snakeContainer}>
      {
        snake.map(({ id, x, y }) => (<div key={id} className={styles.snake}
          style={{
            marginTop: y,
            marginLeft: x
          }}> </div>))
      }
    </section>
  );
};

export default Snake;
